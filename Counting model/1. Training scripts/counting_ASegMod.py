import os 
import numpy as np 
import tensorflow 
import matplotlib.pyplot as plt 
import seaborn as sns
import pandas as pd
os.environ["SM_FRAMEWORK"] = "tf.keras"
import segmentation_models as sm
sm.set_framework("tf.keras")

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Input, GlobalMaxPooling2D, MaxPooling2D, Dense, Flatten, BatchNormalization
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint
from datetime import datetime
from unet import get_unet
from rgcode_train.lib.losses import dice_coef, dice_p_bce, true_positive_rate, euc_dist_keras

import tensorflow.keras.backend as K
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import binary_crossentropy
from skimage.measure import label, regionprops
from tensorflow.keras.callbacks import ModelCheckpoint


# In[2]:


##### Lauching
now = datetime.now()
time = now.strftime("%Y%m%d_%H-%M-%S")
preprocessing = 'Std_Processing_valdicecoef'
condition = 'New_RGCODE_{}'.format(preprocessing)
name = "{}_{}".format(condition,time)
path = "/data/leuven/339/vsc33934/models_counting/{}".format(name)
os.makedirs(path)
print("\nTraining of: ", name)

##### GPU
gpu_train = tensorflow.test.is_gpu_available(cuda_only=True)
print("\nGPU: {}".format(gpu_train))


# In[4]:


Batch_size = 32
Validation_split = 0.25
steps_per_epoch = Batch_size * 4
validation_steps = Batch_size * 4
zoom_range = 0.25
Epochs= 300
WEIGHTS = None
target_size = (128, 128)
SEED= 1111


# In[5]:


image_datagen = ImageDataGenerator(
        rescale=1./255,
        featurewise_center=False,
        featurewise_std_normalization=False,
        rotation_range=90,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        validation_split=Validation_split,
        fill_mode="reflect",
        brightness_range=(0.5, 1.5),   
        zoom_range=zoom_range,                                                    
        cval=0)

mask_datagen = ImageDataGenerator(
        rescale=1./255,
        featurewise_center=False,
        featurewise_std_normalization=False,
        rotation_range=90,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
        validation_split=Validation_split,
        fill_mode="reflect",
        brightness_range=(1.0, 1.0),
        zoom_range=zoom_range,                                                     
        cval=0)


# In[7]:


base_model = sm.Unet('mobilenet', classes= 1, encoder_weights=WEIGHTS)
inp = Input(shape=(128, 128, 1))
l1 = Conv2D(3, (1, 1))(inp) 
out = base_model(l1)

model = tensorflow.keras.Model(inp, out, name=base_model.name)


# In[8]:


model.summary()


# In[11]:


##### Compile and fit model 
model.compile(loss=dice_p_bce, optimizer=Adam(1e-4, decay=1e-6),  metrics=[dice_coef,'binary_accuracy', true_positive_rate, euc_dist_keras])


train_generator = image_datagen.flow_from_directory(
        'DATA/train',
        subset='training',
        target_size=target_size,
        color_mode="grayscale",
        batch_size=Batch_size,
        seed= SEED,
        class_mode=None    
)

# In[14]:


train_mask_generator = mask_datagen.flow_from_directory(
        'DATA/train_masks',
        subset='training',
        target_size=target_size,
        color_mode="grayscale",
        batch_size=Batch_size,
        seed= SEED,
        class_mode=None)


# In[12]:


val_generator = image_datagen.flow_from_directory(
        'DATA/train',
        subset='validation',
        target_size=target_size,
        color_mode="grayscale",
        batch_size=Batch_size,
        seed=SEED,
        class_mode=None
    )


# In[13]:


val_mask_generator = mask_datagen.flow_from_directory(
        'DATA/train_masks',
        subset='validation',
        target_size=target_size,
        color_mode="grayscale",
        batch_size=Batch_size,
        seed=SEED,
        class_mode=None
    )


# In[16]:


train_generator = (pair for pair in zip(train_generator, train_mask_generator))
val_generator = (pair for pair in zip(val_generator, val_mask_generator))


# In[18]:


model_name= "{}/{}_model.hdf5".format(path, name)
checkpoint = ModelCheckpoint(
        model_name,
        monitor='val_dice_coef',
        verbose=1,
        save_best_only=True,
        mode='max',
        save_weights_only =False
        )
callbacks_list = [checkpoint]


# In[ ]:


results = model.fit(
        train_generator,
        steps_per_epoch=steps_per_epoch,
        epochs=Epochs, 
        validation_data=val_generator,
        validation_steps= validation_steps,
        callbacks=callbacks_list
        )

                        
f = open("{}/results_{}.pkl".format(path, name),"wb")
pickle.dump(results.history,f)
f.close()

