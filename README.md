# Deep-learning phenotyping of microglial morphology and heterogeneity

## Description
All code written to design the pipeline meant to phenotype microglial morphology and heterogeneity, in the context of the author's master's thesis, at the Neural Circuit Development and Regeneration research group, headed by Prof. Moons, KU Leuven.

This master's thesis was conducted under the supervision Prof. L. Moons & Prof. Y. Moreau as co-promotors, as well as Dr. L. Andries & L. Masin as supervisors. 

## Models
The best perfoming models for the counting and classification tasks are available [here](https://drive.google.com/drive/folders/1sOS_21ny4jCy0_odd_P8mZ6nzq5MxQfs?usp=sharing). 
