import os 
import numpy as np 
import tensorflow 
import matplotlib.pyplot as plt 
import seaborn as sns
import pandas as pd

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.applications.resnet import ResNet50
from tensorflow.keras.layers import GlobalMaxPooling2D, MaxPooling2D, Dense, Flatten, BatchNormalization
from tensorflow.keras.callbacks import ModelCheckpoint
from datetime import datetime


##### Lauching
now = datetime.now()
time = now.strftime("%Y%m%d_%H-%M-%S")
preprocessing = 'None'
condition = 'RESNET_{}'.format(preprocessing)
name = "{}_{}".format(condition,time)
path = "/data/leuven/339/vsc33934/models/{}".format(name)
os.makedirs(path)
print("\nTraining of: ", name)

##### GPU
gpu_train = tensorflow.test.is_gpu_available(cuda_only=True)
print("\nGPU: {}".format(gpu_train))

##### variables
Batch_size = 32
Validation_split = 0.2
Epochs= 300
WEIGHTS = 'imagenet'


##### Data Generator 
'''gen_args = dict(
        rescale=1./255,
        featurewise_center=False,
        featurewise_std_normalization=False,
        rotation_range=45,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,        
        shear_range=0.3,
        zoom_range=0.3,
        fill_mode="reflect", 
        validation_split=Validation_split
        )'''

gen_args = dict(rescale=1.)
datagen = ImageDataGenerator(**gen_args)


##### Model 
model = Sequential()
model.add(ResNet50(input_shape=(48, 48, 3), weights=WEIGHTS, include_top=False))
model.add(GlobalMaxPooling2D())
model.add(Dense(64, activation="relu"))
model.add(Dense(2, activation="softmax"))

model.summary()


##### Performance metrix 
from tensorflow.keras import backend as K 

def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


##### Compile and fit model 
model.compile(loss='categorical_crossentropy', optimizer= 'Adam', metrics=['accuracy', f1_m])

train_generator = datagen.flow_from_directory(
        'DATA/train', 
        subset='training',
        target_size=(48,48),
        color_mode="rgb",
        batch_size=Batch_size,
        class_mode='categorical') 

validation_generator = datagen.flow_from_directory(
        'DATA/train',  
        subset='validation',
        target_size=(48,48),  
        color_mode="rgb",
        batch_size=Batch_size,
        class_mode='categorical')  

model_name= "{}/{}_model.hdf5".format(path, name)
checkpoint = ModelCheckpoint(
        model_name,
        monitor='val_f1_m',
        verbose=1,
        save_best_only=True,
        mode='max',
        save_weights_only =False
        )
callbacks_list = [checkpoint]

results = model.fit(
        train_generator,
        epochs=Epochs,
        validation_data=validation_generator,
        callbacks=callbacks_list
        )

##### Accuracy & Loss plots
sns.set()
# Plot loss vs epoch 
plt.plot(results.history['loss'])
plt.plot(results.history['val_loss'])
plt.title('model loss')
plt.axis([0, Epochs, 0, 10])
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='best')
plt.savefig('{}/loss_{}.png'.format(path, name))
plt.clf()


# Plot f1-score vs epoch 
sns.set()
plt.plot(results.history['f1_m'])
plt.plot(results.history['val_f1_m'])
plt.title('model f1-score')
plt.axis([0, Epochs, 0, 1])
plt.ylabel('f1-score')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='best')
plt.savefig('{}/fscore_{}.png'.format(path, name))

##### Model evaluation 
del model 

dependencies = {'f1_m': f1_m}
model = tensorflow.keras.models.load_model(model_name, custom_objects = dependencies)

print("\nModel loaded")

model.compile(loss='categorical_crossentropy', optimizer= 'Adam', metrics=['accuracy', f1_m])

test_datagen = ImageDataGenerator(
                rescale= 1./255)

Batch_size=128

test_generator = test_datagen.flow_from_directory(
        'DATA/val',
        subset='training',
        target_size=(48,48),
        color_mode="rgb",
        batch_size=Batch_size,
        class_mode='categorical')

print("Evaluate on test data") 
evalu = model.evaluate(test_generator) 
print("test loss, test acc, test fscore:", evalu)