#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import numpy as np
import pandas as pd
import tensorflow 
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn
import re

from sklearn import metrics
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.saved_model import loader_impl
from tensorflow.python.keras.saving.saved_model import load as saved_model_load
from matplotlib.offsetbox import AnchoredText


# In[2]:


##### variables
#best model C:\Users\arthu\Desktop\Classification\Models\TF-DataAug\RESNET_BestModel_RawData_in_withBrightness_20210422_20-30-10\RESNET_BestModel_RawData_in_withBrightness_20210422_19-38-22_model.hdf5'
Batch_size = 128
model_name = r'C:\Users\arthu\Desktop\Classification\Models_inMT\RESNET_BestModel_RawData_in_withBrightness_20210422_19-38-22\RESNET50_Imagenet_RawData_Brightness0515_20210422_19-38-22_model.hdf5'
#model_pkl = 
path = 'models/'


# In[3]:


from tensorflow.keras import backend as K 

def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


# In[4]:


dependencies = {
    'f1_m': f1_m}
model = tensorflow.keras.models.load_model(model_name, custom_objects = dependencies)

#model.summary()


# In[5]:


model.compile(loss='categorical_crossentropy', optimizer= 'Adam', metrics=['accuracy', f1_m])


# In[6]:


#model.get_weights()


# In[7]:


#model.optimizer


# # Test Script 

# ---

# In[8]:


test_datagen = ImageDataGenerator(
                rescale= 1./255)


# In[9]:


test_generator = test_datagen.flow_from_directory(
        'DATA/val', 
        target_size=(48,48),
        color_mode="rgb",
        batch_size=Batch_size,
        class_mode='categorical',
        shuffle=False)


# In[10]:


# Evaluate the model on the test data
print("Evaluate on test data set")
evalu = model.evaluate(test_generator)
print("test loss:{}, test acc:{}, test fscore:{}".format(evalu[0], evalu[1], evalu[2]))


# In[11]:


# Generate predictions on new data using `predict`
print("Generating predictions...")
predictions = model.predict(test_generator)
model_prediction = predictions.argmax(axis=-1)
print("\nSaving predictions to: ", path, "as a .cvs")


# In[12]:


i=0
pred_1=[]
for i in range(len(model_prediction)):
    pred_1.append(predictions[i][1])
    i=i+1


# In[13]:


def recursive_listdir(folder_path):
    file_list = []
    for root, dirs, files in os.walk(folder_path, topdown=False):
        for name in files:
                file_path = (os.path.join(root, name))
                file_list.append(file_path)
    
    return file_list


# In[14]:


import re
img = recursive_listdir('DATA/val')
labels=[]
for i in range(len(img)):
    result = re.search(r'\d+', img[i])
    labels.append(int(result[0]))
    i=i+1


# In[15]:


pred = model_prediction

pred_x= []
pred_y= []
result= []
#image_name= []

i = 0
for item in labels: 
    pred_x.append(predictions[i][0])
    pred_y.append(predictions[i][1])
    #image_name.append(test_generator.filenames[i])
    if int(pred[i]) == labels[i]:
        result.append('')
    else: 
        result.append('X')
    i = i+1
    

class_prediction = [img, labels, pred_x, pred_y, pred, result]
f = pd.DataFrame(class_prediction)
d = f.transpose()
d.columns = ['img_name','Label', 'Prob_0', 'Prob_1', 'Prediction', 'Error (X)']

class_path = '{}/model_classes.csv'.format(path)
d.to_csv(class_path, index=False)


# ## Roc Curve

# ---

# In[21]:


# roc curve
fpr, tpr, _ = metrics.roc_curve(labels, pred_1)
# AUC
auc = metrics.roc_auc_score(labels, pred_1)
print('AUC: %.3f' % auc)


# In[17]:


# plot the roc curve 
from matplotlib.pyplot import figure
import seaborn as sn
sns.set()
from matplotlib.pyplot import figure
figure(figsize=(8, 8))
plt.plot(fpr, tpr, marker='.')
plt.xlabel('False Positive Rate', fontsize=15)
plt.ylabel('True Positive Rate', fontsize=15)
plt.title('ResNet50 with TL & Brightness level \n', fontsize =15)
plt.fill_between(fpr, tpr, color= 'mediumpurple', alpha=0.2)
plt.legend()
ax = plt.gca()
at = AnchoredText("AUC: {:.3f}".format(auc),
                  prop=dict(size=20), frameon=True,
                  loc='center')
ax.add_artist(at)
plt.show()


# ## Confusion Matrix

# ---

# In[18]:


cm = metrics.confusion_matrix(labels, model_prediction, normalize='true')


# In[19]:


import seaborn as sn
sns.set_context("paper")
df_cm = pd.DataFrame(cm, index = [i for i in "01"],
                  columns = [i for i in "01"])
plt.figure(figsize = (10,7))
sn.heatmap(df_cm, annot=True)
plt.xlabel('Predicted Label')
plt.ylabel('True Laber')
plt.legend()


# In[24]:


import pickle 
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import statistics
from scipy.ndimage.filters import gaussian_filter1d
sns.set()  
model_pkl= r'C:\Users\arthu\Desktop\Classification\Models_inMT\RESNET_BestModel_RawData_in_withBrightness_20210422_19-38-22\results_RESNET_BestModel_RawData_in_withBrightness_20210422_19-38-22.pkl'


# In[26]:


# Plot loss vs epoch 
stats = np.load(model_pkl, allow_pickle=True)
plt.plot(stats['loss'], alpha =0.2)
losssmoothed = gaussian_filter1d(stats['loss'], sigma=25)
plt.plot(losssmoothed, color='blue')
a = stats['val_loss']
a = np.array(a)
av = statistics.median(a)
a[a> 5] = av
plt.plot(a, alpha=0.2)
val_losssmoothed = gaussian_filter1d(a, sigma=25)
plt.plot(val_losssmoothed, color='orange')

xmin = np.argmin(stats["val_loss"])
ymin = np.min(stats["val_loss"])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.plot( np.argmin(stats["val_loss"]), np.min(stats["val_loss"]), marker=".", color="red", label="best model")
plt.vlines(xmin, 0, ymin, colors='red', linestyles='--', label="best model")
plt.legend(['_nolegend_','train','_nolegend_', 'val'], loc='best')
plt.axis([-10, 500, 0, 2])
#plt.savefig('model_loss.png')
plt.show()
plt.clf()


# In[29]:


# Plot accuracy vs epoch 
plt.plot(stats['f1_m'], alpha =0.2)
losssmoothed = gaussian_filter1d(stats['f1_m'], sigma=25)
plt.plot(losssmoothed, color='blue')
plt.plot(stats['val_f1_m'], alpha=0.2)
val_losssmoothed = gaussian_filter1d(stats['val_f1_m'], sigma=25)
plt.plot(val_losssmoothed, color='orange')

xmax = np.argmax(stats["val_f1_m"])
ymax = np.max(stats["val_f1_m"])
plt.title('model f1-score')
plt.ylabel('f1-score')
plt.xlabel('epoch')
plt.plot( np.argmax(stats["val_f1_m"]), np.max(stats["val_f1_m"]), marker=".", color="red")
print(np.max(stats["val_f1_m"]))
plt.vlines(xmax, 0, ymax, colors='red', linestyles='--', label="best model")
plt.legend(['_nolegend_','train','_nolegend_', 'val'], loc='best')
plt.axis([-10, 500, 0, 1])
#plt.savefig('model_accuracy.png')
plt.show()

